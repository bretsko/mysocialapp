//
//  AppDelegate.h
//  SocialApp
//
//  Created by Admin on 10/23/15.
//  Copyright © 2015 bretsko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>

//#pragma clang diagnostic push
//// in reality, you will likely need to disable *more* than Wmultichar
//#pragma clang diagnostic ignored "-Wmultichar"
//#include <TheirLibrary/include.h>
//#pragma clang diagnostic pop
//if you also want to disable the build warnings it generates, then you can use -w or GCC_WARN_INHIBIT_ALL_WARNINGS = YES 

@interface AppDelegate : UIResponder <UIApplicationDelegate, GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign) BOOL loggedIntoFacebook;
@property (assign) BOOL loggedIntoGoogle;

@end

