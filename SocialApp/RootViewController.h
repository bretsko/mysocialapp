//
//  RootViewController.h
//  SocialApp
//
//  Created by Admin on 10/23/15.
//  Copyright © 2015 bretsko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>

@interface RootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

