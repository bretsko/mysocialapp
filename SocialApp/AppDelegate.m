//
//  AppDelegate.m
//  SocialApp
//
//  Created by Admin on 10/23/15.
//  Copyright © 2015 bretsko. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
//Facebook
  [[FBSDKApplicationDelegate sharedInstance] application:application
                           didFinishLaunchingWithOptions:launchOptions];
//Google                           
  NSError* configureError;
  [[GGLContext sharedInstance] configureWithError: &configureError];
  NSAssert(!configureError, @"Error configuring Google services: %@", configureError);
  [GIDSignIn sharedInstance].delegate = self;
  return YES;
}

- (void)toggleAuthUI:(GIDSignIn *)signIn
    didSignInForUser:(GIDGoogleUser *)user
           withError:(NSError *)error {
  // Perform any operations on signed in user here.
 
   // NSString *userId = user.userID;                  // For client-side use only!
 //  NSString *idToken = user.authentication.idToken; // Safe to send to the server
 // NSString *email = user.profile.email;
  
   NSString *name = user.profile.name; 
  // [START_EXCLUDE]
  NSDictionary *statusText = @{@"statusText":
                                 [NSString stringWithFormat:@"Signed in user: %@",
                                  name]};
  [[NSNotificationCenter defaultCenter]
   postNotificationName:@"Google login"
   object:nil
   userInfo:statusText];
  // [END_EXCLUDE]
}

- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
  // Perform any operations on signed in user here.
  NSString *userId = user.userID;                  // For client-side use only!
  NSString *idToken = user.authentication.idToken; // Safe to send to the server
  NSString *name = user.profile.name;
  NSString *email = user.profile.email;
  // [START_EXCLUDE]
  NSDictionary *statusText = @{@"statusText":
                                 [NSString stringWithFormat:@"Signed in user: %@ with email %@, userId %@, and idToken %@",
                                  name, email, userId, idToken]};
  [[NSNotificationCenter defaultCenter]
   postNotificationName:@"Google login"
   object:nil
   userInfo:statusText];   
  
  //  NSDictionary* userInfo = @{@"total": @(messageTotal)};
  
  //  [nc postNotificationName:@"eRXReceived" object:self userInfo:userInfo];
  
}


#pragma mark - Facebook FBSDKAppEvents
- (void)applicationDidBecomeActive:(UIApplication *)application {
// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  [FBSDKAppEvents activateApp];
  //see https://developers.facebook.com/docs/app-events/ios
}

#pragma mark - openURL Facebook and Google
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
  if (self.loggedIntoFacebook) {     
    return [[FBSDKApplicationDelegate sharedInstance] application:application 
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
  }
  //Google, if (self.loggedIntoGoogle)
  else {
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:sourceApplication
                                      annotation:annotation];
  }
}

//- (void)applicationWillResignActive:(UIApplication *)application {
//  // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
//  // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
//}
//
//- (void)applicationDidEnterBackground:(UIApplication *)application {
//  // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
//  // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//}
//
//- (void)applicationWillEnterForeground:(UIApplication *)application {
//  // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
//}

//
//- (void)applicationWillTerminate:(UIApplication *)application {
//  // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
//}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
  // Perform any operations when the user disconnects from app here.
  // [START_EXCLUDE]
  NSDictionary *statusText = @{@"statusText": @"Disconnected user" };
  [[NSNotificationCenter defaultCenter]
   postNotificationName:@"Google login"
   object:nil
   userInfo:statusText];
  // [END_EXCLUDE]
}

@end
