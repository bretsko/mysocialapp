//
//  DataViewController.m
//  SocialApp
//
//  Created by Admin on 10/23/15.
//  Copyright © 2015 bretsko. All rights reserved.
//

//#import "RootViewController.h"
//#import "ModelController.h"
#import "DataViewController.h"
#import "AppDelegate.h"

#import <GoogleSignIn/GoogleSignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface DataViewController ()

@property (strong, nonatomic) FBSDKAccessToken *fbToken;
@property (strong, nonatomic) FBSDKLoginManager *fbLoginManager;
@property (strong, nonatomic) FBSDKGraphRequest *request;
@property (strong, nonatomic) UIImage *imageView;
@property (strong, nonatomic) NSURL *url;

@end

@implementation DataViewController

- (void)viewDidLoad {
  [super viewDidLoad];

#pragma mark - GOOGLE
  [GIDSignIn sharedInstance].uiDelegate = self;
  [GIDSignIn sharedInstance].clientID = @"948849646569-"
      @"o19paum04i2cp500m4m0u5ds6b2d8r7d." @"apps.googleusercontent.com";

  // Uncomment to automatically sign in the user.
  //[[GIDSignIn sharedInstance] signInSilently];
  // [START_EXCLUDE silent]

  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(receiveToggleAuthUINotification)
             name:@"Google login"
           object:nil];

  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(receiveToggleAuthUINotification)
             name:@"Facebook login"
           object:nil];

  [self googleToggleAuthUI];
  [self statusText].text = @"Sign into your account";

#pragma mark - FACEBOOK
  // FACEBOOK

  self.fbLoginManager = [FBSDKLoginManager new];
  //check [FBSDKAccessToken currentAccessToken] before calling logIn* to see if there is a cached token available (typically in your viewDidLoad).
  self.fbToken = [FBSDKAccessToken currentAccessToken];

  // TEST users
  // FBSDKTestUsersManager
  // [FBSDKTestUsersManager sharedInstanceForAppID:404319689773067
  // appSecret:ac9bf7be07e2d4f68b6eb6100c6170a0];

  // CONTENT initialization
  self.content = [FBSDKShareLinkContent new];
  self.content1 = [FBSDKShareLinkContent new];

  self.content.contentURL =
      [NSURL URLWithString:@"http://developers.facebook.com"];

  self.content1.contentURL =
      [NSURL URLWithString:@"https://www.facebook.com/FacebookDevelopers"];

  self.contentURL =
      [[NSURL alloc] initWithString:@"http://en.wikipedia.org/wiki/Facebook"];

  self.imageURL =
      [NSURL URLWithString:@"http://upload.wikimedia.org/wikipedia/commons/"
                           @"thumb/9/95/Facebook_Headquarters_Menlo_Park.jpg/"
                           @"2880px-Facebook_Headquarters_Menlo_Park.jpg"];

#pragma mark - BUTTONS initialization
  //  BUTTONS initialization
  //
  //  Facebook default button
  //  FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
  //  loginButton.center = self.view.center;
  //  [self.view addSubview:loginButton];

  //  Add a custom login button to your app
  //  UIButton *myLoginButton
  //  self.facebookLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
  //  self.facebookLoginButton.frame=CGRectMake(0,0,180,40);
  //  self.facebookLoginButton.center = self.view.center;
  // Add the button to the view
  //  [self.view addSubview:loginButton];

  self.facebookLoginButton.backgroundColor = [UIColor darkGrayColor];
  [self.facebookLoginButton setTitle:@"Log into Facebook "
                            forState:UIControlStateNormal];

  // default settings
  self.facebookLoginButton.readPermissions = @[ @"email", @"user_friends" ];

  //  [self.facebookLoginButton addTarget:self
  //  action:@selector(facebookLoginButtonClicked)
  //  forControlEvents:UIControlEventTouchUpInside];

  self.facebookLikeButton.objectID =
      @"https://www.facebook.com/FacebookDevelopers";
  // Change the style to box count
  //  button.likeControlStyle = FBSDKLikeControlStyleBoxCount;
  // Change the style to box count

  //  button.likeControlHorizontalAlignment =
  //  FBSDKLikeControlHorizontalAlignmentRight;
  //  Next you can use elements like CGRectMake to change the position and size
  //  of the Like button:

  //      FBSDKLikeControl *button = [[FBSDKLikeControl alloc]
  //                                  initWithFrame:CGRectMake(150, 150, 100,
  //                                  50)];
  //      button.likeControlStyle = [FBSDKLikeControlStyleBoxCount];
  //      // Center Button
  //      CGRect bounds = self.view.bounds;
  //      button.center = CGPointMake(CGRectGetMidX(bounds),
  //      CGRectGetMidY(bounds));

  self.facebookShareButton.shareContent = self.content;
  self.facebookSendButton.shareContent = self.content;
}

#pragma mark - facebookLoginButtonClicked
- (IBAction)facebookLoginButtonClicked:(FBSDKLoginButton *)button {

  // Once the button is clicked, show the login dialog

  self.facebookLoginButton.publishPermissions =
      [NSArray arrayWithObjects:@"publish_actions", nil];

  self.facebookLoginButton.defaultAudience = FBSDKDefaultAudienceOnlyMe;

  [self.fbLoginManager logInWithReadPermissions:@[
    @"public_profile"
    // If your app asks for more than public_profile, email and user_friends,
    // Facebook must review it before you release it.
  ] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result,
                                      NSError *error) {
    if (error) {
      self.statusText.text = @"Process error in Facebook login";
    } else if (result.isCancelled) {
      self.statusText.text = @"Cancelled Facebook login";
    } else {
      self.googleLoginButton.hidden = YES;
      self.googleLogOutButton.hidden = YES;
      self.facebookLoginButton.hidden = YES;
      // self.statusText.hidden = YES;
      // NSLog(@"Logged into Facebook");
      // self.statusText.text= @"Logged into Facebook";
    }
  }];

  //[FBSDKApplicationDelegate sharedInstance]

  self.statusText.text = [[FBSDKAccessToken currentAccessToken] userID];

  //[FBSDKAccessToken setCurrentAccessToken:nil]
  // [FBSDKProfile setCurrentProfile:nil].

  //- (IBAction)inputReceived:(UITextField *)sender {
  //
  //    if (!self.emailReceieved){
  //        self.emailReceieved = YES;
  //    }
  //    if (!self.passwordReceieved){
  //        self.passwordReceieved = YES;
  //    }
  //
  //    if (sender.text){
  //        NSString * email = self.InputField.text;
  //    }
  //     [[GIDSignIn sharedInstance].currentUser
  //  GIDSignIn * signIn
  //  GIDGoogleUser * user

  //  [self toggleAuthUI];
  //
  // }
}

- (IBAction)facebookLikeButtonClicked:(FBSDKLikeButton *)sender {

  if (self.fbToken) {
    // TODO:Token is already available.
    [self.fbLoginManager
        logInWithReadPermissions:@[ @"email" ]
              fromViewController:self
                         handler:^(FBSDKLoginManagerLoginResult *result,
                                   NSError *error){
                             // TODO: process error or result
                         }];
  }

  //  if ([self.fbToken hasGranted:@"publish_actions"]) {
  //    // TODO: publish content.
  //  } else {
  //    [self.fbLoginManager logInWithPublishPermissions:@[@"publish_actions"]
  //                                  fromViewController:self
  //                                             handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
  //  TODO: process error or result.
 //                }];
  //  }
}

- (IBAction)facebookShareButtonClicked:(FBSDKShareButton *)button {
  if (self.fbToken) {

    if ([self.fbToken hasGranted:@"publish_actions"]) {
      // TODO: publish content.
    } else {
      [self.fbLoginManager logInWithPublishPermissions:@[
        @"publish_actions"
      ] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result,
                                          NSError *error) {
        // TODO: process error or result.
        if ([result.declinedPermissions containsObject:@"publish_actions"]) {
          // TODO: do not request permissions again immediately. Consider
          // providing a NUX
          // describing  why the app want this permission.
        } else {
        }
      }];
    }

    //  [self.fbLoginManager logInWithReadPermissions:@[@"email"]
    //                             fromViewController:self
    //                                        handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
    //  TODO: process error or result
    //    }];
    //  }

    // Each type of content has a interface you can use to represent it which conforms to . After you model the content, add a sharing interface to your app which conforms to such as FBSDKShareDialog.
    //
    //  Links
    //
    //  When people share links from your app to Facebook, it includes
    //  attributes that show up in the post:
    //
    // contentURL - the link to be shared

    // contentTitle - represents the title of the content in the link imageURL - the URL of thumbnail image that appears on the post contentDescription - of the content, usually 2-4 sentences
    // Build your share content for links with the FBSDKShareLinkContent model.
    // For a list of all attributes, see FBSDKShareLinkContent reference.

    // You can use the example posted below for testing the Share button in your  application.

    //  button.shareContent = [[FBSDKShareLinkContent alloc]
    //                         initWithContentURL: contentURL
    //                         contentTitle: @"My Share Title"
    //                         contentDescription: @"Lorem ipsum dolor sit
    //                         amet."
    //                         imageURL: imageURL
    //                         peopleIDs: @[@"1561082740838259"]

    // User IDs used for peopleIDs must be friends of the current user. You can use the Graph API Explorer to quickly obtain friends IDs. If you do not know about friends permissions please refer to the Facebook Login permissions guide.

    // placeID: @"166793820034304"

    // placeID is currently only available for users having the Facebook app for
    // iOS installed on their device

    // ref: @"myRefId" ];

    // If you are using Simulator to test sharing in your application, you will see errors if you try to share videos, Photos or Open Graph Actions. This is because you need Facebook for iOS installed which provides the Share Dialog. We do not support this for Simulator.

    //  The Share Dialog switches to the native Facebook for iOS app, then returns control to your app after a post is published. If someone doesn't have Facebook app installed it will automatically falls back to a web-based dialog. To call the dialog:
    //
    //    [FBSDKShareDialog showFromViewController:self
    //                                 withContent:content
    //                                    delegate:nil];
    //  To share a link with the Share Dialog:
    //
    //

    //  [FBSDKShareDialog showFromViewController:self
    //                               withContent:content
    //                                  delegate:nil];

    //  FBSDKShareDialog *dialog = [FBSDKShareDialog new];
    //  dialog.fromViewController = self;
    //  // '.fromViewController' is required in order for the share sheet to
    //  present
    //  dialog.content = self.content;
    //  dialog.mode = FBSDKShareDialogModeShareSheet;
    //  [dialog show];

    //  To use a custom interface to share:
    //  1. build a custom interface that posts to the Graph API endpoint
    //  /me/feed
    //  4.  Here's the Graph API call for posting a link to Facebook:
    //    [FBSDKShareAPI shareWithContent:content delegate:nil];

    //  The Message Dialog switches to the native Messenger for iOS app, then returns control to your app after a post is published.
    //
    //    [FBSDKMessageDialog showWithContent:content delegate:nil];
    // Note: Currently the message dialog is not supported on iPads.
  }
}

#pragma mark - fbGraphRequest

- (void)fbGraphRequestFriends {

  // if (self.fbToken) {
  //  FBSDKGraphRequest(graphPath: "me", parameters:
  //  nil).startWithCompletionHandler({ (connection, result, error) -> Void in
  //    println("This logged in user: \(result)")
  //    if error == nil{
  //      if let dict = result as? Dictionary<String, AnyObject>{
  //        println("This is dictionary of user infor getting from facebook:")
  //        println(dict)
  //      }
  //    }
  //  })
  //}

  // For more complex open graph stories, use `FBSDKShareAPI`
  // with `FBSDKShareOpenGraphContent`
  /* make the API call */
  self.request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"/me/friends"
                                                   parameters:nil
                                                   HTTPMethod:@"GET"];
  [self.request
      startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                   id result, NSError *error){
          // Handle the result
      }];

  if (self.fbToken) {
    self.request = [[FBSDKGraphRequest alloc]
        initWithGraphPath:@"me"
               parameters:@{
                 @"fields" : @"id,name,picture.width(100).height(100)"
               }];

    [self.request
        startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                     id result, NSError *error) {
          if (!error) {
            //    NSString *nameOfLoginUser = [result valueForKey:@"name"];
            // NSString *imageStringOfLoginUser = [[[result
            // valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
            // self.url = [[NSURL alloc] initWithURL: imageStringOfLoginUser];
            // get UITableView - TableViewCell
            //   [self.imageView setImageWithURL:url placeholderImage: nil];
          }
        }];
  }

  //  if (self.fbToken) {{
  //  [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
  //  parameters:@{@"fields":
  //  @"email,name,first_name"}]
  //   startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id
  //   result, NSError *error) {
  //     if (!error) {
  //       NSLog(@"fetched user:%@", result);
  //       NSLog(@"%@",result[@"email"]);
  //
  //     }
  //   }];

  // You can append another person's id to the edge in order to determine
  // whether that person is friends with the root node user:

  //  self.request = [[FBSDKGraphRequest alloc]
  //  initWithGraphPath:@"/{user-id-a}/friends/{user-id-b}" parameters:nil
  //  HTTPMethod:@"GET"];
  //  [self.request startWithCompletionHandler:^(FBSDKGraphRequestConnection
  //  *connection,  id result, NSError *error) {
  // Handle the result
  // }];
}

- (void)fbGraphRequest:(NSString *)url {

  // if (self.fbToken) {
  //  FBSDKGraphRequest(graphPath: "me", parameters:
  //  nil).startWithCompletionHandler({ (connection, result, error) -> Void in
  //    println("This logged in user: \(result)")
  //    if error == nil{
  //      if let dict = result as? Dictionary<String, AnyObject>{
  //        println("This is dictionary of user infor getting from facebook:")
  //        println(dict)
  //      }
  //    }
  //  })
  //}

  self.request = [[FBSDKGraphRequest alloc]
      initWithGraphPath:@"me/feed"
             parameters:@{
               @"message" : @"This is a status update"
             }
             HTTPMethod:@"POST"];
  [self.request startWithCompletionHandler:^(FBSDKGraphRequestConnection
                                                 *connection,
                                             id result, NSError *error) {
    if ([error.userInfo[FBSDKGraphRequestErrorGraphErrorCode] isEqual:@200]) {
      NSLog(@"permission error");
    }
  }];

  //  Build your share content for Open Graph actions with the
  //  FBSDKShareOpenGraphContent model. For a list of all attributes, see
  //  FBSDKShareOpenGraphContent reference.
  //
  //    NSDictionary *properties = @{
  //                                 @"og:type": @"fitness.course",
  //                                 @"og:title": @"Sample Course",
  //                                 @"og:description": @"This is a sample
  //                                 course.",
  //                                 @"fitness:duration:value": @100,
  //                                 @"fitness:duration:units": @"s",
  //                                 @"fitness:distance:value": @12,
  //                                 @"fitness:distance:units": @"km",
  //                                 @"fitness:speed:value": @5,
  //                                 @"fitness:speed:units": @"m/s",
  //                                 };
  //  FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject
  //  objectWithProperties:properties];
  //  FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc]
  //  init];
  //  action.actionType = @"fitness.runs";
  //  [action setObject:object forKey:@"fitness:course"];
  //  FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc]
  //  init];
  //  content.action = action;
  //  content.previewPropertyName = @"fitness:course";

  //  if (FBSession.activeSession.isOpen) {
  //
  //    [[FBRequest requestForMe]
  //     startWithCompletionHandler:^(FBRequestConnection *connection,
  //                                  NSDictionary<FBGraphUser> *user,
  //                                  NSError *error) {
  //       if (!error) {
  //         NSString *firstName = user.first_name;
  //         NSString *lastName = user.last_name;
  //         NSString *facebookId = user.id;
  //         NSString *email = [user objectForKey:@"email"];
  //         NSString *imageUrl = [[NSString alloc]
  //                               initWithFormat:
  //                               @"http://graph.facebook.com/%@/picture?type=large",
  //                               facebookId];
  //       }
  //     }];
  //  }

  // Signs the user out of the application for scenarios such as switching
  // profiles.

  // TODO: implement displaying of Facebook name
  //  self.statusText.text = [login ];

  //       if (FBSession.activeSession.isOpen) {
  //
  //         [[FBRequest requestForMe] startWithCompletionHandler:
  //          ^(FBRequestConnection *connection,
  //            NSDictionary<FBGraphUser> *user,
  //            NSError *error) {
  //            if (!error) {
  //              NSString *firstName = user.first_name;
  //              NSString *lastName = user.last_name;
  //              NSString *facebookId = user.objectID;
  //              NSString *email = [user objectForKey:@"email"];
  //              NSString *imageUrl = [[NSString alloc] initWithFormat:
  //              @"http://graph.facebook.com/%@/picture?type=large",
  //              facebookId];
  //            }
  //          }];
  //       }

  //       NSDictionary *statusText = @{@"statusText":
  //                                      [NSString stringWithFormat:@"Signed in
  //                                      user: %@",
  //                                       name]};
  //       [[NSNotificationCenter defaultCenter]
  //        postNotificationName:@"Facebook login"
  //        object:nil
  //        userInfo:statusText];

  // if ([FBSDKAccessToken currentAccessToken]) {
  //  [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
  //  parameters:@{@"fields":
  //  @"email,name,first_name"}]
  //   startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id
  //   result, NSError *error) {
  //     if (!error) {
  //       NSLog(@"fetched user:%@", result);
  //       NSLog(@"%@",result[@"email"]);
  //
  //     }
  //   }];
  //
  //
  //}

  ///* make the API call */
  //[FBRequestConnection startWithGraphPath:@"/me"
  //                             parameters:nil
  //                             HTTPMethod:@"GET"
  //                      completionHandler:^(
  //                                          FBRequestConnection *connection,
  //                                          id result,
  //                                          NSError *error
  //                                          ) {
  //                        /* handle the result */
  //                      }];

  // You can get these information by using the NSDictionary:
  // NSDictionary<FBGraphUser> *user, you just need to use objectforkey to
  // access
  // these values like :
  //
  //[user objectForKey:@"id"],
  //
  //[user objectForKey:@"username"],
  //
  //[user objectForKey:@"email"].

  // FBSession *session = [[FBSession alloc]
  // initWithPermissions:@[@"basic_info",
  // @"email"]];
  //[FBSession setActiveSession:session];
  //
  //[session openWithBehavior:FBSessionLoginBehaviorForcingWebView
  //        completionHandler:^(FBSession *session,
  //                            FBSessionState status,
  //                            NSError *error) {
  //          if (FBSession.activeSession.isOpen) {
  //            [[FBRequest requestForMe] startWithCompletionHandler:
  //             ^(FBRequestConnection *connection, NSDictionary<FBGraphUser>
  //             *user, NSError *error) {
  //               if (!error) {
  //                 NSLog(@"accesstoken %@",[NSString
  //                 stringWithFormat:@"%@",session.accessTokenData]);
  //                 NSLog(@"user id %@",user.id);
  //                 NSLog(@"Email %@",[user objectForKey:@"email"]);
  //                 NSLog(@"User Name %@",user.username);
  //               }
  //             }];
  //          }
  //        }];

  // The SDK will automatically refresh your tokens when you make requests
  // however, in my scenario we send the tokens to our servers and we need to
  // use
  // the latest token. So when our server indicates that we need new tokens this
  // is what we do:
  //
  // Note You can either pass the AppID into the FBSession or you can add the
  // FacebookAppID key to your App's plist (this is what we do).
  //
  //- (void)renewFacebookCredentials {
  //  if (FBSession.activeSession.state == FBSessionStateOpen ||
  //      FBSession.activeSession.state == FBSessionStateOpenTokenExtended) {
  //    [self sessionStateChanged:[FBSession activeSession] state:[FBSession
  //    activeSession].state error:nil];
  //  } else {
  //    // Open a session showing the user the login UI
  //    // You must ALWAYS ask for public_profile permissions when opening a
  //    session
  //    [FBSession
  //    openActiveSessionWithReadPermissions:@[@"public_profile",@"email"]
  //                                       allowLoginUI:NO
  //                                  completionHandler:^(FBSession *session,
  //                                  FBSessionState state, NSError *error) {
  //                                    //this block will run throughout the
  //                                    lifetime of the app.
  //                                    [self sessionStateChanged:session
  //                                    state:state error:error];
  //                                  }];
  //  }
  //}
  // The you can use the sessionStateChanged: method that Facebook include in
  // their documentation but a simplified handler looks like this:
  //
  //- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState)
  //state
  // error:(NSError *)error {
  //  // If the session was opened successfully
  //  NSString *accessToken;
  //  if (!error && state == FBSessionStateOpen && [[session accessTokenData]
  //  accessToken]){
  //    // Show the user the logged-in UI
  //
  //    //@see
  //    http://stackoverflow.com/questions/20623728/getting-username-and-profile-picture-from-facebook-ios-7
  //    accessToken = [[session accessTokenData] accessToken];
  //    //Now we have an access token, can send this to the server...
  //  } else {
  //    //No access token, show a dialog or something
  //  }
  //
  //  //either call a delegate or a completion handler here with the accessToken
  //}
  // Be aware that some of the FBSession API calls check for thread affinity so
  // I
  // found that I had to wrap all my FBSession calls inside a
  // dispatch_async(dispatch_get_main_queue(), ^{...
  //

  // Facebook
  // Get the user's profile picture.

  //[[FBRequest requestForMe] startWithCompletionHandler:^(FBRequestConnection
  //*connection, NSDictionary<FBGraphUser> *FBuser, NSError *error) {
  //  if (error) {
  //    // Handle error
  //  }
  //
  //  else {
  //    NSString *userName = [FBuser name];
  //    NSString *userImageURL = [NSString
  //    stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large",
  //    [FBuser objectID]];
  //  }
  //}];

  // if ([FBSDKAccessToken currentAccessToken]) {
  //  [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{
  //  @"fields"
  //  :
  //  @"id,name,picture.width(100).height(100)"}]startWithCompletionHandler:^(FBSDKGraphRequestConnection
  //  *connection, id result, NSError *error) {
  //    if (!error) {
  //      NSString *nameOfLoginUser = [result valueForKey:@"name"];
  //      NSString *imageStringOfLoginUser = [[[result valueForKey:@"picture"]
  //      valueForKey:@"data"] valueForKey:@"url"];
  //      NSURL *url = [[NSURL alloc] initWithURL: imageStringOfLoginUser];
  //      [self.imageView setImageWithURL:url placeholderImage: nil];
  //    }
  //  }];
  //}

  // if ([FBSDKAccessToken currentAccessToken]) {
  //  [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
  //  parameters:@{@"fields":
  //  @"email,name,first_name"}]
  //   startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id
  //   result, NSError *error) {
  //     if (!error) {
  //       NSLog(@"fetched user:%@", result);
  //       NSLog(@"%@",result[@"email"]);
  //
  //     }
  //   }];

  // let pictureURL =
  // "https://graph.facebook.com/\(fbUserId)/picture?type=large&return_ssl_resources=1"
  ////
  // var URLRequest = NSURL(string: pictureURL)
  // var URLRequestNeeded = NSURLRequest(URL: URLRequest!)
  // println(pictureURL)
  //
  // NSURLConnection.sendAsynchronousRequest(URLRequestNeeded, queue:
  // NSOperationQueue.mainQueue(), completionHandler: {(response:
  // NSURLResponse!,data: NSData!, error: NSError!) -> Void in
  //  if error == nil {
  //    //data is the data of profile image you need. Just create UIImage from
  //    it
  //
  //  }
  //  else {
  //    println("Error: \(error)")
  //  }
  //})
}

- (IBAction)facebookSendButtonClicked:(FBSDKSendButton *)button{
  //  Send Button
  //  The Send button lets people privately send photos, videos and links to
  //  their friends and contacts using the Facebook Messenger. The Send button
  //  will call a Message dialog. To add a Send button to your view add the
  //  following code snippet to your view:

  //  If the Messenger app is not installed, the Send button will be hidden. Be
  //  sure that your app layout is appropriate when this button is hidden. To
  //  inspect whether the Send button can be displayed on the current device use
  //  the FBSDKSendButton property isHidden:
  //
  //  if (button.isHidden) {
  //    NSLog(@"Is hidden");
  //  } else {
  //    [self.view addSubview:button];
  //  }
}

                                      -
                                      (void)
                videoPickerController:(UIImagePickerController *)picker
        didFinishPickingMediaWithInfo:(NSDictionary *)info {
  NSURL *videoURL = [info objectForKey:UIImagePickerControllerReferenceURL];
  // The video URL videoURL must be an asset URL. You can get a video asset URL
  // e.g. from UIImagePickerController.
  FBSDKShareVideo *video = [[FBSDKShareVideo alloc] init];
  video.videoURL = videoURL;
  FBSDKShareVideoContent *content = [[FBSDKShareVideoContent alloc] init];
  content.video = video;

  //...
}

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
  UIImage *image = info[UIImagePickerControllerOriginalImage];

  FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
  photo.image = image;
  photo.userGenerated = YES;
  FBSDKSharePhotoContent *content = [[FBSDKSharePhotoContent alloc] init];
  content.photos = @[ photo ];
  //...
}

- (IBAction)didTapSignOut:(id)sender {

  [[GIDSignIn sharedInstance] signOut];
  // [START_EXCLUDE silent]
  [self googleToggleAuthUI];
  // [END_EXCLUDE]
}

#pragma mark - googleLoginButtonClicked
- (IBAction)googleLoginButtonClicked:(GIDSignInButton *)button {
}

#pragma mark - Google sign in
// Google sign in
- (void)googleToggleAuthUI {
  if ([GIDSignIn sharedInstance].currentUser.authentication == nil) {
    // Not signed in
    self.statusText.text = @"Sign into your account";
    self.googleLoginButton.hidden = NO;
    self.facebookLoginButton.hidden = NO;
    self.googleLogOutButton.hidden = YES;
    //   self.googleDisconnectButton.hidden = YES;

  } else {

    // toggle Alert View to show that signed in
    self.googleLoginButton.hidden = YES;
    self.googleLogOutButton.hidden = NO;
    //  self.googleDisconnectButton.hidden = NO;
    self.facebookLoginButton.hidden = YES;
    self.statusText.hidden = YES;
  }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}

- (void)dealloc {
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:@"Google login"
                                                object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:@"Facebook login"
                                                object:nil];
}

- (void)receiveToggleAuthUINotification:(NSNotification *)notification {
  if ([[notification name] isEqualToString:@"Google login"]) {
    [self googleToggleAuthUI];
    self.statusText.text = [notification userInfo][@"statusText"];
    // NSDictionary* userInfo = notification.userInfo;
    // NSNumber* total = (NSNumber*)userInfo[@"total"];
    // NSLog (@"Successfully received test notification! %i", total.intValue);

//    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"My Alert"
//                                                                   message:@"This is an alert."
//                                                            preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
//                                                          handler:^(UIAlertAction * action) {}];
//    
//    [alert addAction:defaultAction];
//    [self presentViewController:alert animated:YES completion:nil];
//    
    // UIAlertController *alertController = [UIAlertController
    // alertControllerWithTitle:@"Signed-in"
    // message:@"Message"
    // preferredStyle:UIAlertControllerStyleAlert];

    // Adding buttons by creating UIAlertActions:
    // UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
    // style:UIAlertActionStyleDefault
    // handler:nil];

    // You can use a block here to handle a press on this button
    // [alertController addAction:actionOk];
    // [self presentViewController:alertController animated:YES completion:nil];
  } else if ([[notification name] isEqualToString:@"Facebook login"]) {
    // TODO: implement it
  }
}

//- (void)didReceiveMemoryWarning {
//  [super didReceiveMemoryWarning];
//  // Dispose of any resources that can be recreated.
//}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.dataLabel.text = [self.dataObject description];
}

// TODO: Center the buttons
//[NSLayoutConstraint constraintWithItem:label1
// attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
//                                toItem:cell.contentView
//                                attribute:NSLayoutAttributeCenterX
//                            multiplier:1.0 constant:0]
//[NSLayoutConstraint constraintWithItem:label2
// attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
//                                toItem:cell.contentView
//                                attribute:NSLayoutAttributeCenterX
//                            multiplier:1.0 constant:0]
//[NSLayoutConstraint constraintWithItem:label3
// attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual
//                                toItem:cell.contentView
//                                attribute:NSLayoutAttributeCenterX
//                            multiplier:1.0 constant:0]
//
//// Vertical alignment/spacing of 8 between each label
//[NSLayoutConstraint constraintWithItem:label1 attribute:NSLayoutAttributeTop
// relatedBy:NSLayoutRelationEqual
//                                toItem:cell.contentView
//                                attribute:NSLayoutAttributeTop
//                            multiplier:1.0 constant:0]
//[NSLayoutConstraint constraintWithItem:label2 attribute:NSLayoutAttributeTop
// relatedBy:NSLayoutRelationEqual
//                                toItem:label1
//                                attribute:NSLayoutAttributeBottom
//                            multiplier:1.0 constant:8]
//[NSLayoutConstraint constraintWithItem:label3 attribute:NSLayoutAttributeTop
// relatedBy:NSLayoutRelationEqual
//                                toItem:label2
//                                attribute:NSLayoutAttributeBottom
//                            multiplier:1.0 constant:8]

@end
