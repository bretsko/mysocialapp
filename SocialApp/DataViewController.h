//
//  DataViewController.h
//  SocialApp
//
//  Created by Admin on 10/23/15.
//  Copyright © 2015 bretsko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/SignIn.h>

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface DataViewController : UIViewController<GIDSignInUIDelegate>

@property (strong, nonatomic) IBOutlet UILabel *dataLabel;
@property (strong, nonatomic) id dataObject;

@property (weak, nonatomic) IBOutlet GIDSignInButton *googleLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *googleLogOutButton;

@property (weak, nonatomic) IBOutlet UILabel *statusText;

@property (weak, nonatomic) IBOutlet FBSDKLoginButton *facebookLoginButton;

@property (weak, nonatomic) IBOutlet FBSDKShareButton *facebookShareButton;

@property (weak, nonatomic) IBOutlet FBSDKSendButton *facebookSendButton;

@property (weak, nonatomic) IBOutlet FBSDKLikeButton *facebookLikeButton;

@property (strong, nonatomic) FBSDKShareLinkContent *content;
@property (strong, nonatomic) FBSDKShareLinkContent *content1;
@property (strong, nonatomic) NSURL *contentURL;
@property (strong, nonatomic) NSURL *imageURL;


//@property (weak, nonatomic) IBOutlet UITextField *InputField;

@end

