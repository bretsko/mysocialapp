//
//  ModelController.h
//  SocialApp
//
//  Created by Admin on 10/23/15.
//  Copyright © 2015 bretsko. All rights reserved.
//

#import <UIKit/UIKit.h>


@class DataViewController;

@interface ModelController : NSObject <UIPageViewControllerDataSource>

- (DataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;

- (NSUInteger)indexOfViewController:(DataViewController *)viewController;

@end

